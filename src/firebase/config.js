import firebase from "firebase";
import 'firebase/auth'
import 'firebase/firebase'
import 'firebase/storage'

const firebaseConfig = {
    apiKey: "AIzaSyAuUE32B2zsBWSwuGRDJJDvKjLW5FQ3iiw",
    authDomain: "olx-clone-268ba.firebaseapp.com",
    projectId: "olx-clone-268ba",
    storageBucket: "olx-clone-268ba.appspot.com",
    messagingSenderId: "493644221962",
    appId: "1:493644221962:web:a9dba41191b1d8fc9459f3",
    measurementId: "G-XPYMFPZBXC"
  };

 export default firebase.initializeApp(firebaseConfig)